//
//  HomeTableViewCell.swift
//  Renting House
//
//  Created by Kosal Koeung on 10/4/2560 BE.
//  Copyright © 2560 BE CKCC. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var homeDescription: UILabel!
    @IBOutlet weak var imageUrl: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

}
