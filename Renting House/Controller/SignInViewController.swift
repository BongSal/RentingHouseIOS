//
//  SignInViewController.swift
//  Renting House
//
//  Created by Mengsroin Heng on 10/10/17.
//  Copyright © 2017 CKCC. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var UsernameTextField: UITextField!
    
    @IBOutlet weak var PasswordTextField: UITextField!
    
    let username = "ckcc"
    let password = "12345"
    
    @IBAction func OnSignInClick(_ sender: Any) {
        let inputUsername = UsernameTextField.text
        let inputPassword = PasswordTextField.text
        processLogin(username: inputUsername!, password: inputPassword!)
    }
    
    @IBAction func OnCloseBtnClick(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let UserProfileViewController = segue.destination as! UserProfileViewController
        UserProfileViewController.isCloseFromSignIn = true
        UserProfileViewController.LoginButton.isHidden = true
        UserProfileViewController.ViewMyHome.isHidden = false
        UserProfileViewController.NameLabel.text = "Mengsroin"
    }
    
    func processLogin(username: String, password: String){
        let loginUrl = "https://house-testing.000webhostapp.com/user/login"
        let url = URL(string: loginUrl)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let dataString = "username=\(username)&password=\(password)"
        let dataToSubmit = dataString.data(using: .utf8)
        request.httpBody = dataToSubmit
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            self.processLoginResult(data: data)
        }
        task.resume()
    }
    
    func processLoginResult(data: Data?){
        let jsonDict = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String:Any]
        print ("Response: ",jsonDict)
        let responseCode = jsonDict["error"] as? Int
        if (responseCode! == 1){
            print("Login fail")
            let message = jsonDict["error_msg"] as! String
            DispatchQueue.main.async {
                let popupDialog = UIAlertController(title: "Login Fail", message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                popupDialog.addAction(okAction)
                self.present(popupDialog, animated: true, completion: nil)
            }
        } else {
//            let id = jsonDict["_id"] as! Int
//            let name = jsonDict["_name"] as! String
//            let username = jsonDict["_username"] as! String
//            let profilePicture = jsonDict["_profile_picture"] as! String
//            let token = jsonDict["_token"] as! String
//            print("Login success: ")
//            let userJson = jsonDict["user"] as! String
//            let userDict = try! JSONSerialization.jsonObject(with: userJson, options: []) as! [String:Any]
////            let username = userDict["user"] as! String
//            print ("Username : ", userDict )
            // Save user information in UserDefaults
//            UserDefaults.standard.set(id, forKey: "_id")
//            UserDefaults.standard.set(name, forKey: "_name")
//            UserDefaults.standard.set(username, forKey: "_username")
//            UserDefaults.standard.set(profilePicture, forKey: "_profile_picture")
//            UserDefaults.standard.set(token, forKey: "_token")
//            UserDefaults.standard.synchronize()
            
            // Navigate to MainViewController
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "CloseSignInSegue", sender: nil)
            }
        }
    }
    
}
