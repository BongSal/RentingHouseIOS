//
//  Config.swift
//  Renting House
//
//  Created by Kosal Koeung on 10/9/2560 BE.
//  Copyright © 2560 BE CKCC. All rights reserved.
//

import Foundation

let SERVER_URL = "http://house-testing.000webhouseapp.com/"
let HOME_URL = SERVER_URL + "house/get-houses"
