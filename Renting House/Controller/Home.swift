//
//  Home.swift
//  Renting House
//
//  Created by Kosal Koeung on 10/9/2560 BE.
//  Copyright © 2560 BE CKCC. All rights reserved.
//

import Foundation

struct Home {
    let id: Int
    let isActive: String
    let homeName: String
    let imageUrl: String
    let price: Int
    let description: String
    let latitude: String
    let longitude: String
    let address: String
}
