//
//  HomeTableViewController.swift
//  Renting House
//
//  Created by Kosal Koeung on 10/9/2560 BE.
//  Copyright © 2560 BE CKCC. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController {
    
    var homes = [Home]()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadDataFromServer()

    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "home_cell") as! HomeTableViewCell
        let home = homes[indexPath.row]
        
        
        return cell
    }
    
    private func loadDataFromServer() {
        let url = URL(string: "http://house-testing.000webhostapp.com/house/get-houses")!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("Something went wrong !", error!.localizedDescription)
            }
            else {
                
                self.deserializeData(data!)
            }

            
        }
        print("Start loading data")
        task.resume()
    }
    
    func deserializeData(_ data: Data) {
        // Deserialize into Array of Any
        let jsons = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
        print(jsons)
        
        let jsonDatas = jsons["data"] as! [Any]
        
        
        // Loop in Array of Any
        var homes = [Home]()
        
        for jsonData in jsonDatas  {
            
            // Convert object of Any into Dictionary
            let homeDict = jsonData as! [String:Any]
            print(homeDict)
            
            //Fetch each property from Dictionary
            let id = homeDict["id"] as! Int
            let isActive = homeDict["is_active"] as! String
            let homeName = homeDict["house_name"] as! String
            let imageUrl = homeDict["house_image"] as! String
            let address = homeDict["house_address"] as! String
            let latitude = homeDict["latitude"] as! String
            let longitude = homeDict["longitude"] as! String
            
            // Encapsulate properties into an object
            let home = Home(id: id, isActive: isActive, homeName: homeName, imageUrl: imageUrl, price: 1,
                            description: "1234D", latitude: latitude, longitude: longitude, address: address)
            homes.append(home)
        }
        
        self.homes = homes
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }

}
